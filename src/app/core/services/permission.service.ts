import LocalStorage from '../../helpers/localStorage';
import IOptions from '../../interfaces/options.interface';

export class PermissionService {
  static selector = 'permissionService';
  permission: { name: string, options: any }[] = [];
  private storeKey = 'permission';
  constructor(
    private $q: angular.IQService
  ) {
    'ngInject';
  }

  getAll() {
    const permissionStorage = LocalStorage.getItem(this.storeKey);
    if (!!permissionStorage) {
      return this.$q.resolve(permissionStorage);
    }

    const _keys: string[] = ['Calender', 'Profile', 'Property', 'Contacts'];
    const _defaultOptions: IOptions = {
      'view': false,
      'edit': false,
      'remove': false
    };

    _keys.forEach(element => {
      let newItem = {
        name: element,
        options: Object.assign({}, _defaultOptions)
      };
      this.permission.push(newItem);
    });


    LocalStorage.setItem(this.storeKey, this.permission);
    return this.$q.resolve(this.permission);
  }

  save(permission: any) {
    return this.$q.resolve(LocalStorage.setItem(this.storeKey, permission));
  }

}
