import * as angular from 'angular';
import './app.component.scss';
import AppController from './app.controler';


export class App implements angular.IComponentOptions {
  static selector = 'app';
  static template = require('./app.component.html');
  static bindings = {
    permission: '<',
    options: '<'
  };
  static controller = AppController;
}
