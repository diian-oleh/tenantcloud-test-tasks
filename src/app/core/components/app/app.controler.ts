import { PermissionService } from './../../services/permission.service';
import IOptions from '../../../interfaces/options.interface';

export default class AppController {
  permission: any[];
  options: any;
  constructor(
    private permissionService: PermissionService
  ) {
    'ngInject';
  }
  $onInit() {
    this.getPermission();
  }

  calulateOptions() {
    this.options = {
      view: this.checkAllChecked('view'),
      edit: this.checkAllChecked('edit'),
      remove: this.checkAllChecked('remove')
    };
  }

  /**
   * get  permission from storage if exit,
   * else get gefault
   */
  getPermission() {
    this.permissionService.getAll()
      .then(permission => {
        this.permission = permission;
        if (this.permission) {
          this.calulateOptions();
        }
      });
  }

  /**
   * if current permission not enabled it set disable next permissions
   * @param  {number} index
   * @param  {string} name
   */
  onChangePermission(index: number, name: string) {
    const isEnable: boolean = this.permission[index].options[name];
    let options: IOptions = this.permission[index].options;

    if (!isEnable) {
      switch (name) {
        case 'view':
          options.edit = false;
          options.remove = false;
          break;
        case 'edit':
          options.remove = false;
          break;
        default:
          break;
      }
    }
    this.calulateOptions();
  }

  isDisabled(row: any, name: string) {
    let disabled: boolean = false;
    switch (name) {
      case 'edit':
        disabled = !row.view;
        break;
      case 'remove':
        disabled = !(row.edit && row.view);
        break;
      default:
        disabled = false;
        break;
    }

    // if is option disabled set false value
    if (disabled) {
      row[name] = false;
    }
    return disabled;

  }

  checkAll($key: string) {

    const isChecked = this.options[$key];
    if (this.permission.length) {
      this.permission.forEach((element) => {
        let options: any = element.options;
        options[$key] = isChecked;
      });
    }
    this.calulateOptions();
  }

  checkAllChecked(type: string) {
    let isChecked = false;
    if (!this.permission) {
      return isChecked;
    }
    let allValues: boolean[] = [];
    this.permission.forEach((element: any) => {
      const options = element.options;
      allValues.push(options[type]);
    });

    //  if all valuse in column true return true
    return allValues.every((value: boolean) => {
      return value;
    });
  }

  savePermission() {
    this.permissionService.save(this.permission).then(data => {
      alert(data);
    });
  }

}
