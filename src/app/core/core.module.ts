import * as angular from 'angular';

import { App } from './components/app/app.component';
import { PermissionService } from './services/permission.service';

export const moduleName =
  angular.module('application.core', [

  ])
    .component(App.selector, App)
    .service(PermissionService.selector, PermissionService).name;

