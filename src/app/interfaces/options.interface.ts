export default interface IOptions {
  view: boolean;
  edit: boolean;
  remove: boolean;
}
