export default class LocalStorage {

  /**
   * @param  {string} $key
   */
  public static getItem($key: string) {
    try {
      let item = localStorage.getItem($key);
      try {
        let jsonItem = JSON.parse(item);
        return jsonItem;
      } catch (error) {
        return item;
      }
    } catch (error) {
      return false;
    }
  }

  /**
   * @param  {string} $key
   * @param  {any} $value
   */
  public static setItem($key: string, $value: any) {
    try {
      if (Array.isArray($value) || $value instanceof Object) {
        const json = JSON.stringify($value);
        localStorage.setItem($key, json);
      } else {
        localStorage.setItem($key, $value);
      }
      return true;
    } catch (error) {
      return false;
    }
  }

  /**
   * @param  {string} $key
   * @param  {any} $value
   */
  public static pushItem($key: string, $value: any) {
    let arrStore = this.getItem($key);
    if (Array.isArray(arrStore)) {
      arrStore.push($value);
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param  {string} $key
   */
  public static deleteItem($key: string) {
    try {
      localStorage.removeItem($key);
      return true;
    } catch (error) {
      return false;
    }
  }

}
